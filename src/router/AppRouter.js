import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Header from '../components/Header';
import HomePage from '../components/HomePage';
import Gallery from '../components/Gallery';
import NotFoundPages from '../components/NotFoundPages';

const AppRouter = () => {
    <BrowserRouter>
        <div>
            <Header />
            <div className="main-content">
                <Switch>
                    <Route component={HomePage} path="/" exact={true} />
                    <Route component={Gallery} path="/gallery" />
                    <Route component={NotFoundPages} />
                </Switch>
            </div>
        </div>
    </BrowserRouter>
};

export default AppRouter;