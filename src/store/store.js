import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import photosReducer from '../reducers/photos';
import errorsReducer from '../reducers/errors';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    combineReducers({
        photos: photosReducer,
        errors: errorsReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(() => {
    console.log(store.getState());
});

export default store;