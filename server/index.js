const express = require('express');
const cors = require('cors');

const PhotosRouter = require('./routers/photos');
require('./db/connections');

const app = express();
const PORT = process.env.PORT || 3300;

app.use(cors());
app.use(PhotosRouter);

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});